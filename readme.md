# RateCal
Interest rate calculation application
The application is written in Python. Please make sure to install Python runtime and required modules regarding requirement before run the application.

### Requirement
* Python >= 3.6.1
* pandas >= 0.20.1
* numpy >= 1.12.1

### To run application
```python
python ratecal.py [market_file.csv] [loan_amount]

# example
python ratecal.py market_data.csv 100
```

### Running the tests
```python
python -m unittest tests/test_ratecal.py
```

## Author

* **Chanun Chirattikanon** - *Initial work* - <chanun@gmail.com>


import sys
import pandas as pd
import numpy as np


def run():
    try:
        lenders_df = load_lenders(sys.argv[1])
        loan_amount = int(sys.argv[2])
    except (IndexError, ValueError) as e:
        print('Error: ' + str(e))
        print('Please provide correct arguments! \n')
        exit()
    except FileNotFoundError as e:
        print('Error: ' + str(e))
        print('Please provide valid market file! \n')
        exit()
    if len(sys.argv) > 3:
        print('Please provide correct arguments! \n')
        exit()

    total_market_available = lenders_df.iloc[:, 2].sum()

    validate_loan_request(loan_amount, total_market_available)

    [rate, monthly_repay, total_repay] = calculate_lowest_rate_and_repayment(lenders_df, loan_amount)

    print('Requested amount: £%d' % loan_amount)
    print('Rate: {:2.2f}'.format(rate * 100) + '%')
    print('Monthly repayment: £%5.2f ' % monthly_repay)
    print('Total repayment: £%5.2f ' % total_repay)


def load_lenders(market_file):
    return pd.read_csv(market_file, parse_dates=True)


def validate_loan_request(loan_amount, total_market_available):
    if loan_amount < 100 or loan_amount > 15000 or loan_amount % 100 != 0:
        print('Please put Loan amount of £100 increment between £1000 and £15000 inclusive! \n')
        exit()
    if loan_amount > total_market_available:
        print('Sorry, it is not possible to provide a quote at the time! \n')
        exit()


def calculate_repayment(loan_amount, rate):
    no_of_payments = 36
    periodic_interest_rate = float(rate) / 12

    # Discount factor
    d1 = ((1 + periodic_interest_rate) ** no_of_payments) - 1
    d2 = periodic_interest_rate * (1 + periodic_interest_rate) ** no_of_payments
    discount_factor = d1 / d2

    monthly = loan_amount / discount_factor
    total = float(no_of_payments * monthly)

    return [monthly, total]


def calculate_lowest_rate_and_repayment(lenders, loan_amount):
    availables = 0
    monthly_repayment = 0
    total_repayment = 0
    available_loan_amount = loan_amount
    lenders = lenders.sort_values(by='Rate')

    for i in lenders.index:
        if (available_loan_amount - lenders.loc[i].Available) > 0:
            request_loan = lenders.loc[i].Available
            available_loan_amount -= lenders.loc[i].Available
        else:
            request_loan = available_loan_amount

        [monthly_repay, total_repay] = calculate_repayment(request_loan, lenders.loc[i].Rate)
        monthly_repayment += monthly_repay
        total_repayment += total_repay
        availables += lenders.loc[i].Available

        # debug
        # print('-----' + str(i) + '-----')
        # print(lenders.loc[i])
        # print('rate: ' + str(lenders.loc[i].Rate))
        # print('monthly_repay: {:2.2f}'.format(monthly_repay))
        # print('available_amount: {:d} '.format(available_loan_amount))
        # print('monthly_repayments: {:2.2f} '.format(monthly_repayment))
        # print('availables: {:d} '.format(availables))
        # print('-----------')

        if loan_amount <= availables:
            rate = round(12 * np.rate(36, -monthly_repayment, loan_amount, 0.0), 3)
            return [rate, monthly_repayment, total_repayment]


if __name__ == '__main__':
    run()

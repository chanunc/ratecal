
import ratecal
import unittest
import sys
import pandas as pd


class TestRateCalculation(unittest.TestCase):

    def setUp(self):
        self.lenders = pd.read_csv('../market_data.csv', parse_dates=True)

    def test_request_loan_less_than_100(self):
        sys.argv = ['', 'market_data.csv', '10']
        with self.assertRaises(SystemExit) as cm:
            ratecal.run()
        self.assertEqual(cm.exception.code, None)

    def test_request_loan_not_100_increment(self):
        sys.argv = ['', 'market_data.csv', '110']
        with self.assertRaises(SystemExit) as cm:
            ratecal.run()
        self.assertEqual(cm.exception.code, None)

    def test_request_loan_exceed_limit(self):
        sys.argv = ['', 'market_data.csv', '2400']
        with self.assertRaises(SystemExit) as cm:
            ratecal.run()
        self.assertEqual(cm.exception.code, None)

    def test_return_float(self):
        self.assertTrue(isinstance(ratecal.calculate_lowest_rate_and_repayment(self.lenders, 100)[0], float))

    def test_return_rate_with_loan_100(self):
        self.assertEqual(ratecal.calculate_lowest_rate_and_repayment(self.lenders, 100)[0], float('0.069'))

    def test_return_rate_with_loan_500(self):
        self.assertEqual(ratecal.calculate_lowest_rate_and_repayment(self.lenders, 500)[0], float('0.069'))

    def test_return_rate_with_loan_1100(self):
        self.assertEqual(ratecal.calculate_lowest_rate_and_repayment(self.lenders, 1100)[0], float('0.07'))


if __name__ == '__main__':
    unittest.main()